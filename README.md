# Hyper-heuristic approach for Feature Selection

In the feature engineering of any data related project, we often have to filter the columns we will use to train our model manually; this filtering often relies on the insights we have on data and also on many criteria that can help us distinguish between the valuable attributes and the redundant or the meaningless ones, this process is known as the feature selection.

There are many different ways of selecting attributes; the one we used in this repo uses a Ranking feature selection.

Although the ranking methods don't consider the synergies between attributes, These synergies came from the redundancy and complementarity between them, and that's why we used a hyper-heuristic approach to combine these to enhance their performance increase their stability.

You will find in this repository many ranking measures and a genetic meta-heuristic that combines them to generate a composite ranking measure for feature selection.
